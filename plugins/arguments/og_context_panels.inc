<?php

/**
 * @file
 * Plugin to provide an argument handler for all entity IDs and set the OG Context while we are at it.
 */

$plugin = array(
  'title' => t("Entity: OGX"),
  'description' => t('Creates OG context from an entity argument.'),
  'context' => 'og_context_panels_entity_context',
  'get child' => 'og_context_panels_entity_get_child',
  'get children' => 'og_context_panels_entity_get_children',
);

function og_context_panels_entity_get_child($plugin, $parent, $child) {
  $plugins = og_context_panels_entity_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

function og_context_panels_entity_get_children($original_plugin, $parent) {
  $entities = entity_get_info();
  $plugins = array();
  foreach ($entities as $entity_type => $entity) {
    $plugin = $original_plugin;
    $plugin['title'] = t('@entity: OGX', array('@entity' => $entity['label']));
    $plugin['keyword'] = $entity_type;
    $plugin['description'] = t('Creates @entity context from an OGX argument.', array('@entity' => $entity_type));
    $plugin['name'] = $parent . ':' . $entity_type;
    $plugin_id = $parent . ':' . $entity_type;
    drupal_alter('ctools_entity_context', $plugin, $entity, $plugin_id);
    $plugins[$plugin_id] = $plugin;
  }
  drupal_alter('ctools_entity_contexts', $plugins);
  return $plugins;
}

/**
 * Discover if this argument gives us the entity we crave.
 */
function og_context_panels_entity_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  $entity_type = explode(':', $conf['name']);
  $entity_type = $entity_type[1];
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('entity:' . $entity_type);
  }

  // We can accept either an entity object or a pure id.
  if (is_object($arg)) {
    og_context($entity_type, $arg);
    return ctools_context_create('entity:' . $entity_type, $arg);
  }

  if (!is_numeric($arg)) {
    return FALSE;
  }

  $entity = entity_load($entity_type, array($arg));
  og_context($entity_type, $entity[$arg]);

  if (!$entity) {
    return FALSE;
  }

  return ctools_context_create('entity:' . $entity_type, $entity[$arg]);
}
